package nl.utwente.di.bookQuote;

import java.util.HashMap;
import java.util.Map;

public class Quoter {

    private Map<String, Double> bookQuotes = new HashMap<>();

    public Quoter() {
        bookQuotes.put("1", 10.0);
        bookQuotes.put("2", 45.0);
        bookQuotes.put("3", 20.0);
        bookQuotes.put("4", 35.0);
        bookQuotes.put("5", 50.0);
        bookQuotes.put("others", 0.0);
    }

    public double getBookPrice(String isbn) {
        if (bookQuotes.containsKey(isbn)) {
            return bookQuotes.get(isbn);
        } else {
            return 0.0;
        }
    }
}
